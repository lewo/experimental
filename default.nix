let
  sources = import ./nix/sources.nix;
  nixpkgs = sources.nixpkgs;

  # This can be used to patch nixpkgs
  # upstreamPkgs = import nixpkgs {};
  # nixpkgsPatched = upstreamPkgs.stdenv.mkDerivation {
  #   name = "nixpkgs-patched";
  #   src = sources.nixpkgs;
  #   patches = [ ./patches/0001-Revert-systemd-disable-remote-support.patch ];
  #   installPhase = "cp -r ./ $out/";
  #   fixupPhase = ":";
  # };

  pkgs = import nixpkgs {
    config.permittedInsecurePackages = [
      # We need to upgrade to nextcloud 29 (but need to go via 28 to avoid jumping a version)
      "nextcloud-28.0.14"
    ];
    overlays = [
      markasOverlay
      (import ./pkgs/tools.nix)
    ];
  };

  # The server configuration
  configuration = {
    imports = [
      ./configuration/configuration.nix
      (import sources.nixos-mailserver)
    ];
    config = {
      _module.args = {
        inherit sources;
        pkgs = pkgs.lib.mkForce pkgs;
      };
    };
  };

  # The configuration of a VM looking like the server
  vmConfiguration = {
    imports = [
      ./configuration/vm.nix
      (import sources.nixos-mailserver)
      (nixpkgs + "/nixos/modules/virtualisation/qemu-vm.nix")
    ];
    config = {
      _module.args = {
        inherit sources;
        pkgs = pkgs.lib.mkForce pkgs;
      };
    };
  };

  # Make a system from a configuration
  makeSystem = config:
    import (pkgs.path + /nixos) {
      system = "x86_64-linux";
      configuration = config;
    };

  config = (makeSystem configuration).config;

  markasOverlay = self: super: {
    markas-documentation = pkgs.callPackage ./docs {
      inherit sources;
      inherit (self) markas-configuration;
    };
    markas-vm = (makeSystem vmConfiguration).vm;
    markas-vm-configuration = (makeSystem vmConfiguration).config;
    markas-configuration = config;
  };

in rec {
  inherit config;
  vm = pkgs.markas-vm;
  system = (makeSystem configuration).system;
  documentation = pkgs.markas-documentation;
  start-infrastructure = pkgs.markas-start-infrastructure;
  borg-exporter = pkgs.borg-exporter;
  tests = {
    prometheus = pkgs.callPackage ./tests/prometheus.nix { inherit configuration sources; };
    autoUpgrade = pkgs.callPackage ./tests/auto-upgrade.nix { };
    keys = pkgs.callPackage ./tests/keys.nix { };
    vpn = pkgs.callPackage ./tests/vpn.nix { };
    misc = pkgs.callPackage ./tests/misc.nix { inherit configuration; };
    nextcloud = pkgs.callPackage ./tests/nextcloud.nix { inherit configuration; };
    mail = pkgs.callPackage ./tests/mail.nix { inherit configuration; };
    backup = pkgs.callPackage ./tests/backup.nix { };
  };
}
