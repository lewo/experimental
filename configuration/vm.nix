{ config, pkgs, lib, ... }:

with pkgs.lib;

let
  virtualHosts = builtins.attrNames config.services.nginx.virtualHosts;
in
{
  # This adds specific options to run to server configuration in a VM.
  imports = [ ./configuration.nix ../modules/testing.nix ];
  config = {
    boot.loader.grub.device = "/dev/sda";
    fileSystems."/" = {
      device = "/dev/sda";
      fsType = "ext4";
    };

    users.extraUsers.root.password = "";

    # This is to show a French user interface during demo. It would be
    # better to be able to set the french language in Firefox, but i
    # didn't succeed (Firefox needs to be restarted and this fails in
    # our start-infrastructure setup).
    nextcloud.settings.system.force_language = "fr";

    # To allow root access via SSH
    services.openssh = {
      settings = {
        PermitRootLogin = "yes";
        PasswordAuthentication = mkForce true;
      };
      extraConfig = "PermitEmptyPasswords yes";
    };

    # On the server, the anonymous mode is enable. When the anonymous
    # mode is enable, it is not possible to create dashboard. In
    # the VM, we disable the authentication mode to allow
    # interactive dashboard creations.
    services.grafana.settings."auth.anonymous".enabled = mkForce false;

    # It takes too much resource when it starts in the VM, and it is
    # not really useful in the VM context.
    services.autoUpgrade.enable = mkForce false;

    networking.firewall.enable = mkForce false;

    # We don't have the VPN private key in a dev environment.
    services.vpn.member.enable = false;

    services.prometheus.alertmanager.logLevel = "debug";

    mailserver.check.enable = mkForce false;

    # We create values for expected Nextcloud passwords
    keys.nextcloud-admin.path = mkForce (toString (pkgs.writeTextFile {
      name = "nextcloud.password";
      text = "admin";
    }));

    networking.extraHosts = ''
      127.0.0.1 ${concatStringsSep " " virtualHosts}
    '';

    environment = {
      # This file can be used to manuall push borg metrics to pushgateway
      # cat /etc/borg/borg-output-sample.json | borg-exporter http://localhost:9091
      etc."borg/borg-output-sample.json".source = ../data/borg/borg-output-sample.json;
      systemPackages = [ pkgs.borg-exporter ];
    };

    mailserver = {
      enable = true;
      loginAccounts = {
        # This account is only used to create a new account from the
        # VM in order to show the registration process.
        "utilisateur@markas.fr" = {
          hashedPassword = "should-be-a-hash";
        };
      };
    };

    virtualisation = {
      diskSize = 12 * 1024;
      memorySize = 5 * 1024;
      msize = 32 * 1024;
      cores = 3;
      # To access the services of the VM from your host
      forwardPorts = [
        { from = "host"; host.port = 2222; guest.port = 22; }
        { from = "host"; host.port = 3000; guest.port = 3000; }
        { from = "host"; host.port = 9090; guest.port = 9090; }
        { from = "host"; host.port = 9100; guest.port = 9100; }
        { from = "host"; host.port = 9093; guest.port = 9039; }
        { from = "host"; host.port = 30443; guest.port = 443; }
      ];
    };
  };
}

