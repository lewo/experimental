{ pkgs, lib, config, ...}:

let
  cfg = config;
  # This is to add a label instance sets with the address as value
  relabel_configs = [
    {
      source_labels = [ "__address__" ];
      target_label = "instance";
      regex = "([^:]+).*";
    }
  ];
in
{
  services.nginx.virtualHosts = {
    "metriques.markas.fr" = {
      forceSSL = true;
      enableACME = true;
      locations."/".proxyPass = "http://localhost:9090";
    };
    "alertes.markas.fr" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:9093";
        extraConfig = ''
          limit_except GET {
          deny  all;
          }
        '';
      };
    };
  };

  # This is because the nextcloud admin password file is owned by the
  # nextcloud user and the nextcloud exporter needs to access it since
  # it is using the nextcloud admin account to get metrics from nextcloud.
  systemd.services.prometheus-nextcloud-exporter = {
    serviceConfig.User = pkgs.lib.mkForce "nextcloud";
    # If we wait for nextcloud-setup we are sure the nextcloud admin key
    # has been created and the exporter can connect to nextcloud
    after = [ "nextcloud-setup.service" ];
    requires = [ "nextcloud-setup.service" ];
  };

  services.prometheus = {
    enable = true;
    scrapeConfigs = [
      {
        inherit relabel_configs;
        job_name = "prometheus";
        scrape_interval = "5s";
        static_configs = [{ targets = ["localhost:9090"]; }];
      }
      {
        inherit relabel_configs;
        job_name = "node";
        scrape_interval = "15s";
        static_configs = [{ targets = ["localhost:9100"]; }];
      }
      {
        inherit relabel_configs;
        job_name = "pushgateway";
        scrape_interval = "15s";
        honor_labels = true;
        static_configs = [{ targets = ["localhost:9091"]; }];
      }
      {
        inherit relabel_configs;
        job_name = "nextcloud";
        scrape_interval = "90s";
        static_configs = [{ targets = ["localhost:9205"]; }];
      }
    ];
    # TODO: we need to add more rules /:
    ruleFiles = [ ../data/prometheus/node.rules ];
    # TODO: we need to define exporters we want
    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
      };
    } // lib.optionalAttrs cfg.services.nextcloud.enable {
      nextcloud = {
        enable = true;
        url = "https://${cfg.services.nextcloud.hostName}";
        username = cfg.services.nextcloud.config.adminuser;
        passwordFile = cfg.services.nextcloud.config.adminpassFile;
      };
    };

    alertmanagers = [
      {
        static_configs = [
          { targets = [ "localhost:9093" ]; }
        ];
      }
    ];

    alertmanager = {
      enable = config.services.prometheus.enable;
      listenAddress = "localhost";
      configuration = {
        global = {
          smtp_from = "monitoring@markas.fr";
          smtp_smarthost = "localhost:25";
        };
        receivers = [
          {
            name = "mail";
            email_configs = [
              {
                to = "admin@markas.fr";
                tls_config.insecure_skip_verify = true;
              }
            ];
          }
        ];
        route.receiver = "mail";
      };
    };

    pushgateway = {
      enable = true;
    };
  };
}
