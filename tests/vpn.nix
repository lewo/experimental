{ pkgs, git, wireguard-tools }:

let

  mlib = import ./lib.nix { inherit pkgs; };

  keyServer = pkgs.runCommand "key-server" { buildInputs = [ wireguard-tools ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  keyMember = pkgs.runCommand "key-member" { buildInputs = [ wireguard-tools ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  server = {pkgs, config, ...}: {
      imports = [ ../modules/vpn.nix ];
      config = {
        # TODO: this should not be part of this test but of the full test
        # instead.
        services.journald.enableHttpGateway = true;

        services.vpn.member = {
          clients = [{
            publicKey = mlib.readPublicKey "${keyMember}/public";
            ip = "10.100.0.2";
          }];
          privateKeyFile = "${keyServer}/private";
        };
      };
  };

  member = {pkgs, config, ...}: {
    config = {
      networking.wireguard.interfaces = {
        wg0 = {
          ips = [ "10.100.0.2/24" ];
          listenPort = 51820;
          privateKeyFile = "${keyMember}/private";
          peers = [
            {
              publicKey = mlib.readPublicKey "${keyServer}/public";
              allowedIPs = [ "10.100.0.1" ];
              endpoint = "server:51820";
              # This is to initialize the connection to the
              # server. Without this, the server can not communicate
              # to the client until the client sends a first request.
              persistentKeepalive = 25;
            }
          ];
        };
      };
    };
  };

in

pkgs.nixosTest {
  name = "test-vpn";
  nodes = { inherit server member; };
  testScript = ''
    server.wait_for_unit("multi-user.target")
    member.wait_for_unit("multi-user.target")
    server.wait_until_succeeds("ping -c 1 10.100.0.2")
  '';
}
