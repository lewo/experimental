# This test checks the full configuration of the server
#
# Implemented tests are:
# - journalctl is reachable through the VPN

{ configuration, pkgs, git, wireguard-tools }:

let

  keyServer = pkgs.runCommand "key-server" { buildInputs = [ wireguard-tools ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  keyMember = pkgs.runCommand "key-member" { buildInputs = [ wireguard-tools ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  mlib = import ./lib.nix { inherit pkgs; };

  rabi = {pkgs, lib, config, ...}: {
    imports = [
      configuration
      ./common.nix
    ];

    config = {
      virtualisation = {
        diskSize = 1 * 1024;
        memorySize = 2 * 1024;
      };

      networking.extraHosts = ''
        127.0.0.1 documentation.markas.fr alertes.markas.fr
        '';

      services.vpn.member = {
        clients = [{
          publicKey = mlib.readPublicKey "${keyMember}/public";
          ip = "10.100.0.254";
        }];
        # We override the server public key for the test
        privateKeyFile = lib.mkForce "${keyServer}/private";
      };
    };
  };
  
  member = {pkgs, config, ...}: {
    config = {
      networking.wireguard.interfaces = {
        wg0 = {
          ips = [ "10.100.0.254/24" ];
          listenPort = 51820;
          privateKeyFile = "${keyMember}/private";
          peers = [
            {
              publicKey = mlib.readPublicKey "${keyServer}/public";
              allowedIPs = [ "10.100.0.1" ];
              endpoint = "rabi:51820";
              # This is to initialize the connection to the
              # server. Without this, the server can not communicate
              # to the client until the client sends a first request.
              persistentKeepalive = 25;
            }
          ];
        };
      };
    };
  };
in
pkgs.nixosTest {
  name = "test-misc";
  nodes = { inherit rabi member; };
  testScript = ''
    rabi.wait_for_unit("multi-user.target")
    member.wait_for_unit("multi-user.target")

    # Check if journald is exposed over the vpn
    member.succeed("curl 10.100.0.1:19531")
    member.fail("curl --max-time 4 rabi:19531")

    # Nginx
    member.succeed("curl -k --max-time 4 https://rabi")

    # The port 80 is used by acme
    member.succeed("curl --max-time 4 http://rabi")

    rabi.succeed(
        "curl -L -k https://documentation.markas.fr | \
        grep 'Infrastructure collaborative markas.fr'"
    )

    # POST request are not allowed on alertes.markas.fr.
    # Otherwise, anyone can set Alertmanager silences :/
    rabi.succeed(
        "curl -k -L -H 'Content-Type: application/json' -X POST -d '{}' \
         https://alertes.markas.fr/api/v1/silences 2>&1  | \
         grep '403'"
    )
  '';
}
