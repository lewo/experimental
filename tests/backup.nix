# This test is composed by two machines, the server and a backup
# target machine with a ssh account for borg.
#
# This test:
# - creates a file in the server backup directory
# - backups this directory on the target
# - extracts this file from the backup archive on the target
# - ensures the file can not be read on the target

{ pkgs }:

let
  privateKey = pkgs.writeText "id_ed25519" ''
    -----BEGIN OPENSSH PRIVATE KEY-----
    b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
    QyNTUxOQAAACBx8UB04Q6Q/fwDFjakHq904PYFzG9pU2TJ9KXpaPMcrwAAAJB+cF5HfnBe
    RwAAAAtzc2gtZWQyNTUxOQAAACBx8UB04Q6Q/fwDFjakHq904PYFzG9pU2TJ9KXpaPMcrw
    AAAEBN75NsJZSpt63faCuaD75Unko0JjlSDxMhYHAPJk2/xXHxQHThDpD9/AMWNqQer3Tg
    9gXMb2lTZMn0pelo8xyvAAAADXJzY2h1ZXR6QGt1cnQ=
    -----END OPENSSH PRIVATE KEY-----
  '';
  publicKey = ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHHxQHThDpD9/AMWNqQer3Tg9gXMb2lTZMn0pelo8xyv root@client
  '';
  passphrase = pkgs.writeText "borgbackup-passphrase" "passphrase";

  backupDir = "/var/backup";
  server = {pkgs, config, ...}: let
    cfg = config;
  in {
    imports = [ ../modules/backup.nix ../modules/keys.nix ];
    config = {
      environment.systemPackages = [ cfg.services.backup.borgCommand ];

      services = {
        prometheus.pushgateway.enable = true;
        backup = {
          enable = true;
          passphrasePath = toString passphrase;
          sshPrivateKeyPath = "/var/keys/borgbackup-sshprivatekey/id_ed25519";
          paths = [backupDir];
          repo = "borg@target:.";
        };
      };
    };
  };

  target = {pkgs, config, ...}: {
    config = {
      services.openssh = {
        enable = true;
      };
      services.borgbackup.repos.markas = {
        authorizedKeys = [ publicKey ];
      };
    };
  };
in
pkgs.nixosTest {
  name = "test-backup";
  nodes = { inherit server target; };
  testScript = ''
    server.wait_for_unit("multi-user.target")
    server.succeed("mkdir -p /var/keys/borgbackup-sshprivatekey")
    server.succeed(
        "cp ${privateKey} /var/keys/borgbackup-sshprivatekey/id_ed25519"
    )
    server.succeed("chmod 0600 /var/keys/borgbackup-sshprivatekey/id_ed25519")

    server.succeed("mkdir -p ${backupDir}")
    server.succeed("echo file1 > ${backupDir}/file1")

    target.wait_for_unit("multi-user.target")

    # Run the backup job
    server.systemctl("start borgbackup-job-remote.service")

    # Wait until an archive is created on the target
    server.wait_until_succeeds(
        "borg list --last 1 --short borg@target:. | grep -q server-remote"
    )

    # Extract the archive from the target and ensure files are correct
    archive_name = server.succeed("borg list --last 1 --short borg@target:.").rstrip()
    server.succeed("borg extract borg@target:.::{}".format(archive_name))
    server.succeed("ls ./var/backup/file1")
    server.succeed("[ $(cat ./var/backup/file1) == file1 ]")

    # Ensure the files can not be read on the target
    target.succeed("ls /var/lib/borgbackup/data")
    target.fail(
        "find /var/lib/borgbackup/data -exec ${pkgs.binutils-unwrapped}/bin/strings | grep -q file1"
    )

    server.wait_until_succeeds("curl -s localhost:9091/metrics | grep borg_nfiles")
  '';
}
