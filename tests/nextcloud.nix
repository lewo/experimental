{ pkgs, configuration }:


let
  adminPassword = "admin";
  memberPassword = "deiph1shee5eiSa";
  mlib = import ./lib.nix { inherit pkgs; };
in

pkgs.nixosTest {
  name = "test-nextcloud";
  nodes = {
    machine = {pkgs, lib, config, ...}: {
      imports = [
        configuration
        ./common.nix
      ];
      config = {
        virtualisation = {
          diskSize = 4 * 1024;
          memorySize = 3 * 1024;
        };

        # We create values for expected Nextcloud passwords
        keys.nextcloud-admin.path = lib.mkForce (toString (pkgs.writeTextFile {
          name = "nextcloud.password";
          text = "admin";
        }));

        networking.extraHosts = ''
          127.0.0.1 cloud.markas.fr
          127.0.0.1 office.markas.fr
        '';

        nextcloud = {
          enable = lib.mkForce true;
          adminEmail = lib.mkForce "admin@markas.fr";
          settings.system.mail_smtpstreamoptions.ssl = {
            allow_self_signed = true;
            verify_peer = false;
            verify_peer_name = false;
          };
        };

        mailserver = {
          enable = lib.mkForce true;
          loginAccounts = {
            "admin@markas.fr" = {
              hashedPasswordFile = mlib.hashPassword adminPassword;
            };
            "member@markas.fr" = {
              hashedPasswordFile = mlib.hashPassword memberPassword;
            };
          };
          enableImapSsl = true;
          localDnsResolver = false;
          check.enable = lib.mkForce false;
        };

        environment.systemPackages = with pkgs; [
          firefox-unwrapped geckodriver
        ];

      };
    };
  };

  testScript = let

    withRcloneEnv = pkgs.writeScript "with-rclone-env" ''
      #!${pkgs.stdenv.shell}
      export RCLONE_CONFIG_NEXTCLOUD_TYPE=webdav
      export RCLONE_CONFIG_NEXTCLOUD_URL="https://cloud.markas.fr/remote.php/dav/files/member"
      export RCLONE_CONFIG_NEXTCLOUD_VENDOR="nextcloud"
      export RCLONE_CONFIG_NEXTCLOUD_USER="member"
      export RCLONE_CONFIG_NEXTCLOUD_PASS="$(${pkgs.rclone}/bin/rclone obscure ${memberPassword})"
      "''${@}"
    '';

    copySharedFile = pkgs.writeScript "copy-shared-file" ''
      #!${pkgs.stdenv.shell}
      echo 'hi' | ${withRcloneEnv} ${pkgs.rclone}/bin/rclone --no-check-certificate rcat nextcloud:test-shared-file
    '';

    diffSharedFile = pkgs.writeScript "diff-shared-file" ''
      #!${pkgs.stdenv.shell}
      diff <(echo 'hi') <(${pkgs.rclone}/bin/rclone --no-check-certificate cat nextcloud:test-shared-file)
    '';

    requestAccount = pkgs.writers.writePython3Bin "request-account" {
      libraries = [ pkgs.python3Packages.selenium ];
    } ''
      from selenium.webdriver import Firefox
      from selenium.webdriver.firefox.options import Options
      from selenium.webdriver.common.by import By

      options = Options()
      options.add_argument('--headless')
      driver = Firefox(options=options)

      driver.implicitly_wait(20)
      driver.get('https://cloud.markas.fr')
      assert "Nextcloud" in driver.title

      driver.find_element(By.LINK_TEXT, 'Register').click()

      email_input = driver.find_element(By.NAME, "email")
      email_input.send_keys("member@markas.fr")
      driver.find_element(By.ID, "submit").click()
    '';

    createAccount = pkgs.writers.writePython3Bin "create-account" {
      libraries = [ pkgs.python3Packages.selenium ];
    } ''
      import sys

      from selenium.webdriver import Firefox
      from selenium.webdriver.firefox.options import Options
      from selenium.webdriver.common.by import By

      # Verification url sent by email when someone register on nextcloud
      verify_url = sys.argv[1]

      options = Options()
      options.add_argument('--headless')
      driver = Firefox(options=options)

      driver.implicitly_wait(20)
      driver.get(verify_url)

      username_input = driver.find_element(By.NAME, "loginname")
      username_input.send_keys("member")

      password_input = driver.find_element(By.NAME, "password")
      password_input.send_keys("${memberPassword}")

      driver.find_element(By.ID, "submit").click()
    '';

  in ''
    rabi.wait_for_unit("multi-user.target")

    rabi.wait_for_unit("nextcloud-setup.service")

    rabi.succeed("curl -Lk https://cloud.markas.fr/login")

    # Request a new account creation for member@markas.fr
    # Unix user member will receive a verification link by mail
    rabi.succeed(
        "${requestAccount}/bin/request-account"
    )
    # Read received email in member mailbox to get verify url
    url = rabi.succeed(
        "${mlib.mailCheck} read --show-body --ignore-dkim-spf --imap-username member@markas.fr --imap-password ${memberPassword} Nextcloud | grep 'Continue registration' | sed 's/Continue registration: //'"
    )
    # Go to verify url and create account
    rabi.succeed(
        "${createAccount}/bin/create-account %s"
        % url
    )
    # Check that admin user received notification about member user account creation
    rabi.succeed(
        "${mlib.mailCheck} read --ignore-dkim-spf --imap-username admin@markas.fr --imap-password ${adminPassword} Nextcloud"
    )
    # Check member user account is disabled
    rabi.succeed("nextcloud-occ user:info member | grep 'enabled: false'")
    # Enable the user
    rabi.succeed("nextcloud-occ user:enable member")

    # Check webdav with member user
    rabi.succeed(
        "${withRcloneEnv} ${copySharedFile}"
    )
    rabi.succeed(
        "${withRcloneEnv} ${diffSharedFile}"
    )

    # Check if calendar and contacts app are available
    # Calendar
    rabi.succeed(
        "curl -k --user member:${memberPassword} -X PROPFIND https://cloud.markas.fr/remote.php/dav/calendars/member/personal/"
    )
    rabi.succeed(
        'curl -k -H "Content-Type: text/calendar; charset=utf-8" --user member:${memberPassword} -X PUT --data-binary @${./files/event.ics} https://cloud.markas.fr/remote.php/dav/calendars/member/personal/TJPXZI0CGOUO625762Z79Q.ics'
    )

    # Contacts
    rabi.succeed(
        'curl -k -H "Content-Type: text/vcard; charset=utf-8" --user member:${memberPassword} -X PUT --data-binary @${./files/contact.vcf} https://cloud.markas.fr/remote.php/dav/addressbooks/users/member/contacts/880abbed-2828-45c7-adfe-7329602140d8.vcf'
    )
    rabi.succeed(
        "curl -k --user member:${memberPassword} -X GET https://cloud.markas.fr/remote.php/dav/addressbooks/users/member/contacts/880abbed-2828-45c7-adfe-7329602140d8.vcf"
    )

    # Do a backup of the mysql database
    rabi.start_job("mysql-backup.service")
    # Wait for backup file and check that data appears in backup
    rabi.wait_for_file("/var/backup/mysql/nextcloud.gz")
    rabi.succeed(
        "${pkgs.gzip}/bin/zcat /var/backup/mysql/nextcloud.gz | grep member"
    )

    # Ensure the websocket is responding through NGinx
    # TODO: we should send real WOPI message instead :/
    rabi.succeed("echo foo | ${pkgs.websocat}/bin/websocat wss://office.markas.fr/lool/foo/ws -k")
  '';
}
