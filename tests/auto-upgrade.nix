# In this test we create a gpg key pair and a repository containing a
# default.nix file building a dummy switch-to-configuration script.
# The repository initally contains one commit which is signed
# with the created gpg key.
#
# The VM boots, checks out this repository and build the commit and
# execute the dummy switch-to-configuration script. We then add
# another commit which is not signed and we check this commit is not
# built.

{ pkgs, git, gnupg }:

let
  key = pkgs.runCommand "key" { buildInputs = [ gnupg ]; } ''
    export GNUPGHOME=$PWD
    cat >details <<EOF
      %echo Generating a basic OpenPGP key
      Key-Type: RSA
      Key-Length: 2048
      Subkey-Type: RSA
      Subkey-Length: 2048
      Name-Real: user1
      Name-Comment: user1
      Name-Email: user1@test.com
      Expire-Date: 0
      %no-ask-passphrase
      %no-protection
      # Do a commit here, so that we can later print "done" :-)
      %commit
      %echo done
    EOF
    gpg --verbose --batch --generate-key details
    mkdir $out
    gpg --armor --export user1 > $out/public
    gpg --armor --export-secret-keys user1 > $out/private
  '';

  default = pkgs.writeTextFile {
    name = "default.nix";
    text = ''
      let pkgs = import ${pkgs.path} {};
      in {system = pkgs.runCommand "switch-to-configuration" {} "mkdir -p $out/bin; echo touch /tmp/commit1 > $out/bin/switch-to-configuration; chmod a+x $out/bin/switch-to-configuration";}
    '';
  };
  # We build the default.nix file to file the store of the
  # VM. Otherwise, it can't build it since it doesn't have access to
  # the network.
  defaultBuilt = {
    system = pkgs.runCommand "switch-to-configuration" {} "mkdir -p $out/bin; echo touch /tmp/commit1 > $out/bin/switch-to-configuration; chmod a+x $out/bin/switch-to-configuration";
  };

  repository = pkgs.runCommand "repository" { buildInputs = [ git gnupg ]; } ''
    # GPG configuation
    export HOME=$PWD
    gpg --import ${key}/public
    gpg --import ${key}/private

    mkdir $out
    cd $out
    git init
    git config user.email user1@test.com
    git config user.name user1

    git config user.signingKey user1
    cp ${default} default.nix
    git add default.nix
    git commit -a -m "commit1" -S
  '';

in
pkgs.nixosTest {
  name = "test-auto-upgrade";
  nodes = {
    machine = {pkgs, config, ...}: {
      imports = [ ../modules/auto-upgrade.nix ];
      config = {
        services.autoUpgrade = {
          enable = true;
          remotes = [ "/tmp/non-existing-repository" "/tmp/repository" ];
          keys = [ "${key}/public" ];
        };
        environment.systemPackages = [ defaultBuilt.system ];
      };
    };
  };
  testScript = ''
    machine.wait_for_unit("multi-user.target")
    machine.succeed("systemctl stop auto-upgrade.timer")
    machine.succeed(
        "cp -r ${repository} /tmp/repository"
    )

    # We first verify we can deploy the signed commit
    machine.succeed("systemctl restart auto-upgrade.service")
    # TODO: do not use wait_until_succeeds
    # I don't know how to be sure the systemd service is running.
    machine.wait_until_succeeds("ls /tmp/commit1")

    # We commit a non signed commit and check it is not deployed
    machine.succeed(
        "${pkgs.git}/bin/git -C /tmp/repository commit --allow-empty -m commit-not-signed"
    )
    machine.succeed("systemctl restart auto-upgrade.service")
    # TODO: use a timestamp to be sure we are reading message from the latest call
    machine.wait_until_succeeds(
        "journalctl -u auto-upgrade.service | grep -q 'Could not verify the signature'"
    )
  '';
}
