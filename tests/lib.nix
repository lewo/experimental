{ pkgs }:

{

  hashPassword = password: pkgs.runCommand
    "password-${password}-hashed"
    { buildInputs = [ pkgs.mkpasswd ]; }
    ''
      mkpasswd -m sha-512 ${password} > $out
    '';

  mailCheck = "${pkgs.python3}/bin/python ${../scripts/mail-check.py}";

  readPublicKey = filepath: builtins.replaceStrings ["\n"] [""] (builtins.readFile filepath);

}
