let
  nixpkgs = (import ./nix/sources.nix).nixpkgs;
  pkgs = import nixpkgs {};
in
pkgs.mkShell {
  buildInputs = with pkgs; [
  (python3.withPackages(p: [p.sphinx p.recommonmark p.sphinx_rtd_theme]))
  niv
  sops
  ];
}
