{ pkgs, config, ...}:

{
  systemd.services.test-weekly-alertmanager = {
    description = "Weekly Alertmanager test";
    wantedBy = [ "multi-user.target" ];
    wants = [ "alertmanager.service" ];
    after = [ "alertmanager.service" ];
    restartIfChanged = false;
    unitConfig.X-StopOnRemoval = false;
    startAt = "weekly";
    path = [ pkgs.bash pkgs.curl pkgs.netcat ];
    script = ''
      timeout 10 bash -c 'until nc -z localhost 9093; do sleep 1; done'

      curl -H "Content-Type: application/json" \
           -d '[{"labels":{"alertname":"TestWeeklyAlertmanager"}}]' \
           localhost:9093/api/v1/alerts
    '';
  };
}
