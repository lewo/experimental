{ pkgs, config, ... }:
with pkgs.lib;
let
  cfg = config.services.vpn;
in
{
  imports = [ ./keys.nix ];
  options = {
    services.vpn.member = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = "Whether to enalbe The Member VPN";
      };

      clients = mkOption {
        description = "submodule example";
        type = with types; listOf (submodule {
          options = {
            publicKey = mkOption {
              type = types.str;
              description = "The public key of the client.";
            };
            ip = mkOption {
              type = str;
              description = "The IP of the client.";
            };
          };
        });
      };
      privateKeyFile = mkOption {
        type = types.str;
        default = "/var/keys/wireguard-member";
        description = "The public key of the machine hosting an alertmanager.";
      };
    };
  };

  config = {
    networking = mkIf cfg.member.enable {
      firewall = {
        allowedUDPPorts = [ 51820 ];
        extraCommands = ''
          # To allow members to connect to the journald web service
          iptables -I INPUT -s 10.100.0.1/24 -p tcp --dport 19531 -i wg-member -j ACCEPT
        '';
      };
      wireguard.interfaces = {
        wg-member = {
          ips = [ "10.100.0.1/24" ];
          listenPort = 51820;
          # Path to the private key file.
          privateKeyFile = cfg.member.privateKeyFile;
          peers = map (c: {
            publicKey = c.publicKey;
            allowedIPs = [ c.ip ];
          }) cfg.member.clients;
        };
      };
    };

    # FIXME: we should also disable this option when the member vpn is
    # disable (in the vm)
    #
    # The Wireguard module adds a systemd path unit file on this
    # path. So, we don't need to manually set a dependency on it. We
    # only add this to explicitly declare this secret in our
    # configuration.
    keys.wgPrivateKeyMember.path = cfg.member.privateKeyFile;
  };
}
