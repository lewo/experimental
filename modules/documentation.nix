{ pkgs, config, ...}:

{
  services.nginx = {
    enable = true;
    virtualHosts = {
      "documentation.markas.fr" = {
        forceSSL = true;
        enableACME = true;
        locations."/".root = pkgs.markas-documentation;
      };
      "markas.fr" = {
        forceSSL = true;
        enableACME = true;
        locations."/".root = pkgs.markas-documentation;
      };
    };
  };
}
