{ pkgs, config, lib, ... }:
with lib;
let
    cert = pkgs.runCommand "cert" {} ''
      mkdir $out
      ${pkgs.minica}/bin/minica \
        --ca-key $out/key.pem \
        --ca-cert $out/cert.pem \
        --domains selfsigned.local

      chmod 600 $out/*
   '';
in
{
  options = {
    testing = {
      ca = mkOption {
        type = types.package;
        default = cert;
        readOnly = true;
        description = "The directory containing the root CA used by acme";
      };
    };
  };

  config = {
    # We override the ACME initialisation by putting a CA generated at
    # build time. This allows us to preinstall this CA in firefox.
    systemd.services.acme-selfsigned-ca.script = mkForce ''
      mkdir -p ca
      cp ${cert}/* ca/
    '';
    security.pki.certificates = [ (builtins.readFile "${cert}/cert.pem") ];

    systemd.services.nextcloud-setup.script = ''
      nextcloud-occ security:certificates:import ${cert}/cert.pem
    '';

    virtualisation.oci-containers.containers.collabora.extraOptions = [
      "--add-host" "cloud.markas.fr:172.17.0.1"
    ];
  };

}
