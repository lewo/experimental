.. _reglement_interieur:

Règlement intérieur
-------------------


Article 1 – Champs d’application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le présent règlement concerne l’association Markas. Ce règlement
s’adresse à tous les membres adhérents de l’association Markas. Il
s’applique à partir de 2021 et n’est pas rétroactif.

Article 2 – Statut des membres
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’association compte deux types de membres :

-  les membres fondateurs sont les signataires des statuts lors de la
   constitution de l’association. Ce statut est valable sans autre
   limitation de durée que celle de l’association.
-  les membres actifs, personnes adhérentes ou affiliées, ont une activité
   régulière et identifiée au sein de l’association. Ce statut est
   valable un an, du 1er janvier au 31 décembre de l’année d’adhésion du
   membre. Les personnes morales peuvent adhérer à l’association, mais
   doivent en ce cas désigner une personne physique qui servira de
   contact référent.

La liste des membres est publique pour l’ensemble des membres de
l’association.

.. _RI-cotisation:

Article 3 – Cotisation
^^^^^^^^^^^^^^^^^^^^^^

Les montants des cotisations sont fixés comme suit :

-  l’adhésion et l’affiliation sont à prix libre, à partir du minimum
   symbolique de 1 €. Le prix conseillé sera déterminé à chaque début
   d’année par les frais annuels prévisionnels de fonctionnement divisés
   par le nombre d’adhérents à la fin de l’exercice précédent. Une
   personne adhérent à l’association en cours d’année est libre de
   cotiser au prorata des mois restants.
-  les membres fondateurs sont dispensés de cotisation, sauf s’ils sont
   aussi membres actifs.

La cotisation liée au renouvellement d’une adhésion doit être effectuée
avant mars. Sinon, la personne adhérente/affiliée sera considérée comme
démissionnaire et sa radiation pourra être prononcée par la Collégiale
conformément aux statuts.

Les cotisations ne seront pas remboursées.

Article 4 – Adhésion
^^^^^^^^^^^^^^^^^^^^

Une personne souhaitant faire partie de l’association doit être cooptée
par au moins un des membres de la Collégiale. Le point d’entrée se fait
via la demande de création d’un compte sur le système cloud du serveur
de l’association, validée par un des membres de la Collégiale. La
Collégiale dispose alors d’un délai de sept jours pour exprimer une
éventuelle opposition, qui sera motivée autant que possible. Une telle
décision devra être prise par vote au sein de la Collégiale, à la
majorité simple.

Pour obtenir le statut d’adhérent, la personne entrante doit :

-  prendre connaissance des statuts de l’association et du présent règlement intérieur,
-  fournir ses informations de contact : nom, prénom et adresse,
-  effectuer le paiement de la cotisation qui doit avoir lieu dans
   les 30 jours suivant la création du compte en ligne. Si aucun paiement
   n’est parvenu à échéance, l’adhésion est considérée comme nulle.
   L’association accuse réception de l’encaissement par l’envoi d’un reçu,
   qui pourra être électronique.

Conformément à la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux
fichiers et aux libertés, l’adhérent dispose d’un droit d’accès et de
rectification des données le concernant auprès de la Collégiale. L’adhérent
s’engage à porter à la connaissance de l’association toute modification portant
sur ses informations de contact.

Article 5 – Vote et prise de décision
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour rappel :

-  majorité simple : la proposition qui obtient le nombre de voix le
   plus élevé est adoptée,
-  majorité absolue : la proposition est adoptée si elle recueille au moins la
   moitié des votes plus une voix,
-  le vote blanc est l’abstention,
-  le vote nul est celui qui se fait de manière irrégulière.

En dehors des Assemblées Générales et des réunions de la Collégiale, les
décisions se font à la majorité simple, sans quorum. Sur propositions,
elles peuvent se faire à la majorité absolue ou sur un autre mode dont
les modalités devront être clairement précisées. De la même façon, si
les circonstances l’imposent, une prise de décision impliquant un quorum
peut être mise en place, à condition que les modalités en soient
clairement précisées.

Les votes qui auront lieu de façon numérique devront être bornés dans le
temps et comporter une date de début et une date de fin de recueil des
voix.

Les membres actifs prennent part aux votes. Les membres fondateurs ont
une voix consultative.

Article 6 – Fonctionnement
^^^^^^^^^^^^^^^^^^^^^^^^^^

Tout acte ou prestation effectué au nom de l’association, par l’un de
ses membres, devra être autorisé par la Collégiale. Si l’acte ou la
prestation au nom de l’association est rétribué, il ne pourra pas donner
lieu à rétribution personnelle, l’association étant dans ce cas le seul
bénéficiaire autorisé, par un règlement transmis à la Collégiale.

Aucune prise de position publique ne peut se faire au nom de l’association sans
accord préalable ou délégation de pouvoir de la Collégiale.

Article 7 – Code de conduite
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tout membre de l’association s’engage à respecter les statuts et le
règlement intérieur de l’association. Un adhérent, par ses actions ou
ses déclarations, ne doit pas entraîner un préjudice moral ou matériel
à l’association. Si ce préjudice est estimé comme grave par la
Collégiale, l’adhérent peut être radié de l’association. Le RFC 1855
(Netiquette : http://fgouget.free.fr/netiquette/rfc1855-fr.html)
s’applique au sein de l’association, et tout manquement grave à cette
Netiquette ou au règlement intérieur entraînera des sanctions pouvant aller
jusqu’à l’exclusion de l’adhérent.
