Infrastructure collaborative markas.fr
=======================================

Markas a pour objectif de fournir des services numériques respectant
nos libertés, nos données personnelles et notre vie privée. Les
services proposés sont détaillés dans le :ref:`guide d'utilisation
<utiliser>` de Markas.

.. container:: account

   :ref:`Créer un compte utilisateur <compte>`


Nos motivations: pourquoi Markas?
---------------------------------

Nous construisons Markas pour contribuer à `l'écosystème
<https://chatons.org/fr/find>`_ permettant de nous défaire de
l'omniprésence des `GAFAM
<https://fr.wikipedia.org/wiki/GAFAM>`_. Nous avons donc pour souhait
de rejoindre `le Collectif des Hébergeurs Alternatifs, Transparents,
Ouverts, Neutres et Solidaires <https://chatons.org>`_ et nous allons
proposer notre candidature dans les mois à venir.

L'infrastructure Markas est gérée collaborativement. Nous sommes
particulièrement attachés à la notion de **transparence** et
d\'**ouverture**.

La **transparence** permet à n'importe quel utilisateur de savoir comment
est construite l'infrastructure et quelles sont les actions qui y sont
réalisées. La majorité des actions sont réalisées à travers `notre
dépôt Git <https://framagit.org/markas/infrastructure>`_ dont
`l'historique est publique
<https://framagit.org/markas/infrastructure/-/commits/master/>`_.

L\'**ouverture** vise à permettre aux utilisateurs de contribuer au
projet. Les utilisateurs doivent donc pouvoir étudier, reproduire et
modifier l'infrastructure. Nous fournissons un moyen simple permettant
d\':ref:`essayer le serveur localement <essayer>`. Afin de permettre
aux utilisateurs de contribuer à l'infrastructure, toutes les
modifications sont réalisées via des `demandes de fusion
<https://framagit.org/markas/infrastructure/-/merge_requests>`_. Sur
le long terme, nous visons l'absence de droits d'administration. Voir
la section :ref:`contribuer` pour davantage de détails.


Sommaire
--------

.. toctree::
   :maxdepth: 2

   association.rst
   utiliser.rst
   description.rst
   contribuer.rst
   administration.rst
   contact.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
