Administration de l’infrastructure
==================================

Cette section documente les opérations d’administration de
l’infrastructure.

Mise à jour de la distibution NixOS
-----------------------------------

Le serveur est basé sur la release |nixpkgs-branch| et nous utilisons
l'outil `Niv <https://github.com/nmattia/niv>`__ pour mettre à jour la
revision de NixOS utilisée:

::

   niv update nixpkgs

Il faut ensuite lancer les tests et commiter les fichiers modifiés.

Cette opération est actuellement manuelle mais pourra être effectuée par
la CI.

Procédure d’installation de NixOS sur Kimsufi
---------------------------------------------

Voir
https://lewo.abesis.fr/posts/2019-12-01-install-nixos-on-kimsufi.html

Signature des commits
---------------------

Pour n’avoir que des commits signés et ne pas faire confiance à Gitlab,
les MR sont mergées en "fast forward".

Deux conditions sont nécessaires pour apporter des contributions signées:

- signer ses commits. Pour cela, suivre la `documentation de git
  <https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work>`_. On pourra
  aussi ajouter la configuration suivante pour signer par défaut les commits,
  sans devoir préciser le paramètre ``-S`` lors de la création de ceux-ci :

::

   git config commit.gpgsign true

- ajouter sa clé GPG **publique** (et surtout pas la privée) dans le répertoire
  `./keys <https://framagit.org/markas/infrastructure/-/tree/master/keys>`_
  *via* une MR.


Secrets
-------

Le module
`./modules/keys.nix <https://framagit.org/markas/infrastructure/-/tree/master/modules/keys.nix>`__
permet de définir des secrets utilisés par d’autres modules. Chaque
secret est spécifié dans un fichier. La création de ce fichier est
laissé à l’utilisateur (généralement créé via ssh ou scp).

Lorsque qu’un secret est défini dans la configuration, un service
systemd est créé pour observer le fichier contenant ce secret. Ce
service systemd peut alors être utilisé comme dépendance d’un module
ayant besoin du secret contenu dans ce fichier.

Le test
`./tests/keys.nix <https://framagit.org/markas/infrastructure/-/tree/master/tests/keys.nix>`__
illustre cette fonctionnalité. Dans ce test, un secret ``foo`` est
utilisé par un service systemd ``test``. Tant que le fichier
``/run/keys/foo`` n’est présent, systemd bloque le démarrage du service
``test``. Une fois le fichier ``/run/keys/foo`` créé, le service
``test`` est démarré.

Monitoring
----------

Les services de monitoring sont exposés publiquement.

- `Grafana <https://grafana.markas.fr>`_
- `Prometheus <https://metriques.markas.fr>`_
- `Alertmanager <https://alertes.markas.fr>`_

VPN
---

Un VPN est utilisé pour accéder certains des services de
l’infrastructure:

-  journald: http://vpn.markas.fr:19531

Ce VPN est basé sur Wireguard. La clé publique du VPN est
``jngKolQu7KZUw+vqKUjDRHF3C9PO8nnly69lPg8pbX0=``.

Un exemple de configuration cliente (pour NixOS) est disponible dans le
`test <https://framagit.org/markas/infrastructure/-/tree/master/tests/vpn.nix>`__.
Cette configuration est à adapter en fonction de votre distribution. Il
faut également soumettre une demande fusion avec votre clé Wireguard
publique.

Note: le VPN rend votre machine utilisateur accessible depuis le
serveur. Il est donc préférable d’avoir un firewall!

Secrets administratifs
----------------------

Divers secrets sont centralisés et chiffrés avec l'outil `sops
<https://github.com/mozilla/sops>`__ dans le fichier `secrets.yaml` à la racine
du dépôt.

::

    # Éditer le fichier
    sops secrets.yaml

    # Afficher le contenu
    sops -d secrets.yaml


Les personnes habilitées à lire ces secrets sont référencées dans le fichier
`.sops.yaml`.  Pour ajouter ou supprimer une personne il faut mettre à jour la
liste des fingerprint pgp dans ce fichier et lancer la commande :

::

    sops updatekeys secrets.yaml


Gestion des adhérents
---------------------

Mail de bienvenue pour les nouveaux adhérents
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::

   **Informations à adapter :**

   - nom de l'adhérent (à la place de "X"),
   - montant de la cotisation conseillée si changement (:ref:`ici dans la doc
     <assoc-cotisation>`).

   **Ajouter en pièce jointe :**

   - RIB

::

    Subject: Bienvenue chez Markas

    Bonjour X,

    Bienvenue dans l'association Markas !

    Votre compte a été activé sur le cloud https://cloud.markas.fr.
    Il est maintenant possible de l'utiliser.

    Les informations relatives à l'association sont consultables ici : https://markas.fr.

    Vous disposez d'une période d'essai d'un mois. Au delà de cette
    période, pour continuer à utiliser les services de Markas, vous
    devez adhérer à l'association. Pour adhérer, nous vous invitons à :
    - consulter les statuts et le règlement intérieur dans la section
      "L'association Markas" et les conditions générales d'utilisation dans la
      section "Guide d'utilisation". Votre adhésion implique leur acceptation,
    - compléter vos informations de contact dans votre profil sur le cloud :
      - menu en haut à droite (icône avec la première lettre de votre prénom) >
        Paramètres > Informations personnelles,
      - informations minimales à renseigner : nom, prénom et adresse,
    - payer la cotisation :
      - montant minimal = 1 €/an, prix conseillé = 20 €/an,
      - de préférence par virement bancaire : RIB en pièce jointe. Indiquer
        vos nom et prénom dans le libellé du virement,
      - plus d'informations ici : https://markas.fr/association.html#cotisation.

    N'hésitez pas à nous contacter pour toute question, et à vous inscrire à
    notre liste de diffusion ou participer à nos groupes de discussion sur
    Matrix ou IRC (informations ici : https://markas.fr/contact.html) !

    À bientôt,

    La collégiale de Markas.


Installation NixOS sur KS-11 (RAID)
-----------------------------------

Kimsufi ne propose pas d'image NixOS. Nous devons donc passer par le
système de rescue pour installer NixOS. Une fois un NixOS basique
installer, nous déployons la configuration Markas.

L'article `How to install NixOS on kimsufi
<https://lewo.abesis.fr/posts/2019-12-01-install-nixos-on-kimsufi.html>`__
décrit ce qu'il faut faire pour activer le mode Rescue dans la webui
Kimsufi. Dans la suite, nous fournissons les commandes shell pour
installer NixOS depuis le mode rescue.

.. code::

    # Partitionning GPT
    parted -s /dev/sda -- mklabel gpt
    parted -s /dev/sda -- mkpart bios_grub 1MiB 2MiB
    parted -s /dev/sda -- set 1 bios_grub on
    parted -s /dev/sda -- mkpart ext4 2Mib 514MiB
    parted -s /dev/sda -- mkpart ext4 514MiB -8GiB
    parted -s /dev/sda -- mkpart linux-swap -8GiB 100%

    parted -s /dev/sdb -- mklabel gpt
    parted -s /dev/sdb -- mkpart bios_grub 1MiB 2MiB
    parted -s /dev/sdb -- set 1 bios_grub on
    parted -s /dev/sdb -- mkpart ext4 2Mib 514MiB
    parted -s /dev/sdb -- mkpart ext4 514MiB -8GiB
    parted -s /dev/sdb -- mkpart linux-swap -8GiB 100%

    mdadm --create /dev/md126 --level=1 --raid-devices=2 /dev/sda2 /dev/sdb2 --metadata=0.90
    mdadm --create /dev/md127 --level=1 --raid-devices=2 /dev/sda3 /dev/sdb3

    mkfs.ext4 -L boot /dev/md126
    mkfs.ext4 -L nixos /dev/md127
    mkswap -L swap /dev/sda4

    mount /dev/disk/by-label/nixos /mnt
    swapon /dev/sda4

    mkdir /mnt/boot
    mount /dev/disk/by-label/boot /mnt/boot

    # Create user
    groupadd -g 30000 nixbld
    useradd -u 30000 -g nixbld -G nixbld nixbld

    curl -L https://nixos.org/nix/install | sh
    . /root/.nix-profile/etc/profile.d/nix.sh
    nix-channel --add https://nixos.org/channels/nixos-21.05 nixpkgs
    nix-channel --update
    nix-shell -p nixos-install-tools -p vim

    nixos-generate-config --root /mnt

    # Add the following lines to the NixOS configuration
    boot.loader.grub.devices = [ "/dev/sda" "/dev/sdb" ];
    # For booting with /boot on raid
    boot.loader.grub.extraGrubInstallArgs = [ "--modules=nativedisk ahci pata part_gpt part_msdos diskfilter mdraid1x lvm ext2" ];
    # Necessary to boot with a degraded array
    boot.initrd.mdadmConf = ''
       <use configuration generated by `mdadm -Es`. Remove any --name option.>
       '';
    # SSH access after reboot
    services.openssh.enable = true;
    users.extraUsers.root.openssh.authorizedKeys.keys = [
       "<ssh-pub-key>"
    ];

    nixos-install --root /mnt

Reconfiguration après un changement de disque
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Après démarrage de la machine le statut du RAID devrait ressembler à ça:

.. code::

    # cat /proc/mdstat
    Personalities : [raid1]
    md127 : active raid1 sdb3[1]
          1944468544 blocks super 1.2 [2/1] [_U]
          bitmap: 12/15 pages [48KB], 65536KB chunk

    md126 : active raid1 sdb2[1]
          524224 blocks [2/1] [_U]

    unused devices: <none>

Ici, c'est le disque `sda` qui a été changé. Il suffit de le partitionner
comme à l'origine:

.. code::

    nix-shell -p parted
    parted -s /dev/sda -- mklabel gpt
    parted -s /dev/sda -- mkpart bios_grub 1MiB 2MiB
    parted -s /dev/sda -- set 1 bios_grub on
    parted -s /dev/sda -- mkpart ext4 2Mib 514MiB
    parted -s /dev/sda -- mkpart ext4 514MiB -8GiB
    parted -s /dev/sda -- mkpart linux-swap -8GiB 100%

D'ajouter les paritions au RAID:

.. code::

    # mdadm --add /dev/md127 /dev/sda3
    mdadm: added /dev/sda3
    # mdadm --add /dev/md126 /dev/sda2
    mdadm: hot added /dev/sda2

Le RAID devrait alors être en cours de reconstruction:

.. code::

    # cat /proc/mdstat
    Personalities : [raid1]
    md127 : active raid1 sda3[2] sdb3[1]
          1944468544 blocks super 1.2 [2/1] [_U]
          [>....................]  recovery =  0.3% (7502848/1944468544) finish=204.1min speed=158114K/sec
          bitmap: 12/15 pages [48KB], 65536KB chunk

    md126 : active raid1 sda2[2] sdb2[1]
          524224 blocks [2/1] [_U]
            resync=DELAYED

Pour finir, réinstaller la configuration Nix pour que grub soit réinstallé
dans le MBR:

.. code::

   cd /var/nixos
   nix-build -A system
   ./result/bin/switch-to-configuration switch
