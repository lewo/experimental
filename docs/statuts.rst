.. _statuts:

Statuts
-------

- Votés lors de l'Assemblée Générale Constitutive le jeudi 4 février 2021.
- Validés par le service public le lundi 26 avril 2021.


Article 1 - Titre de l’association
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'association Markas est régie par les articles 21 à 79-III du Code
Civil Local maintenu en vigueur dans les départements du Bas-Rhin, du
Haut-Rhin et de la Moselle, ainsi que par les présents statuts.

L’association est inscrite au registre des associations du tribunal
d’Instance de Colmar.

Cette association poursuit un but non lucratif.

Article 2 - But de l’association
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’association a pour vocation de :

-  maintenir une infrastructure numérique fournissant des services
   respectant la vie privée des utilisateurs,
-  communiquer afin de promouvoir l’utilisation des logiciels libres et
   des services numériques respectueux des utilisateurs,
-  favoriser la contribution des utilisateurs à l’infrastructure.

Article 3 - Siège social
^^^^^^^^^^^^^^^^^^^^^^^^

Le siège social est fixé au 20, route des Bagenelles, 68650 Le
Bonhomme. Il pourra être transféré sur décision de l’assemblée en
collégiale.

L’adresse de correspondance est également fixée au 20, route des
Bagenelles, 68650 Le Bonhomme.

Article 4 - Composition de l’association
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’association se compose de personnes adhérentes et de personnes
affiliées.

Est reconnue personne adhérente toute personne physique adhérant aux
statuts et règlement intérieur en vigueur, dûment inscrite et à jour de
sa cotisation.

Est reconnue personne affiliée toute personne morale légalement
constituée adhérant aux statuts et règlement intérieur en vigueur,
dûment inscrite et à jour de sa cotisation.

La personne affiliée est une entité juridique collective en la personne
exclusive d’une personne la représentant, et détenant les mêmes
caractéristiques qu’une personne adhérente.

Toute personne adhérente ou affiliée a le droit de participer à
l’Assemblée Générale avec voix délibérative, la capacité de se porter
volontaire à l’assemblée en collégiale (« la Collégiale ») et
conséquemment solidaire en responsabilité juridique.

Toute personne adhérente ou affiliée volontaire à la Collégiale peut à
tout moment suspendre ou mettre fin à sa participation par simple
notification écrite à la Collégiale, qui en avisera l’Assemblée Générale
par le biais de sa communication habituelle.

Les régimes de cotisation sont prévus au règlement intérieur.

Article 5 - Admission
^^^^^^^^^^^^^^^^^^^^^

La décision d’adhérer revient à la personne adhérente ou à la personne
affiliée qui satisfait aux conditions sus-désignées.

Article 6 - Radiation
^^^^^^^^^^^^^^^^^^^^^

La qualité de membre se perd par :

-  la démission,
-  le décès de la personne physique ou la dissolution de la personne
   morale,
-  la radiation prononcée par la Collégiale pour non-paiement de la
   cotisation, pour infraction aux statuts ou pour motif portant
   préjudice aux intérêts moraux et matériels de l’association, ou pour
   motif grave.

Le règlement intérieur pourra préciser quels sont les motifs graves.

La radiation d’une personne adhérente ou affiliée volontaire à la
Collégiale est possible, de manière temporaire ou définitive. Cette
radiation est soumise au vote des personnes adhérentes et des personnes
affiliées volontaires à la Collégiale depuis plus d’un an et nécessite
2/3 des voix pour être validée.

Article 7 - Les ressources de l’Association
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Elles comprennent :

-  le montant des cotisations, les dons,
-  les subventions de l’Union Européenne, de l’État, des régions, des
   départements et communes, ou de tout autre organisme public,
-  les sommes perçues en contrepartie des prestations fournies par
   l’Association,
-  toutes les autres ressources autorisées par les textes législatifs ou
   réglementaires.

Article 8 - La Collégiale
^^^^^^^^^^^^^^^^^^^^^^^^^

L’association est dirigée par une Collégiale composée d’au moins deux
personnes adhérentes ou affiliées et de toutes les personnes adhérentes
ou affiliées qui en expriment explicitement la volonté, sauf en cas de
radiation les privant de leur droit d’accès à la Collégiale pour une
période définie ou indéfinie.

Toutes les personnes qui participent à la Collégiale peuvent contribuer
librement au bon fonctionnement administratif, comptable et technique de
l’association à hauteur de leurs disponibilité, affinités et
compétences.

Les personnes représentant les personnes affiliées qui rejoindraient la
Collégiale ne peuvent représenter qu’une seule voix en son sein. Si une
personne adhérente ou affiliée volontaire à la Collégiale représente
plusieurs personnes, elle ne peut représenter qu’une voix.

Article 9 - Solidarité en responsabilité juridique
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sont reconnues légalement solidaires en représentation de l’association
l’ensemble des personnes participant à la Collégiale.

Toute personne adhérente ou affiliée souhaitant effectuer une action
nécessitant une représentation juridique de la Collégiale se doit, pour
ce faire, de rejoindre la Collégiale sauf dérogation exceptionnelle.

Article 10 - Réunion de la Collégiale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La Collégiale se réunit au moins une fois l’an, à sa discrétion mais sur
communication à l’Assemblée Générale, ou à la demande du quart des
personnes adhérentes et affiliées y participant.

La présence et la participation décisionnaire des personnes volontaires
à la Collégiale peut être sous forme électronique, conformément aux
dispositions prévues par le Règlement Intérieur.

La prise de décision se fait au consensus des voix exprimées.

Toute personne volontaire à la Collégiale reste libre des modalités de
sa participation, sans que son absence puisse entraver ou suspendre en
aucune façon le processus décisionnaire.

Bien que la liste des personnes participant à la Collégiale soit en
permanence sujette à changement, celle-ci est présentée au vote lors de
l’Assemblée Générale Ordinaire pour validation par les personnes
constituant ladite Assemblée. Si l’Assemblée Générale se prononce contre
la présence d’une personne adhérente ou affiliée dans la collégiale,
cette personne est radiée de la Collégiale jusqu’à la prochaine
Assemblée Générale.

Article 11 - Pouvoirs de la Collégiale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La Collégiale est investie des pouvoirs les plus étendus dans les
limites de l’objet de l’association et dans le cadre des résolutions
adoptées par l’Assemblée Générale. Elle peut autoriser tous actes ou
opérations qui ne sont pas statutairement de la compétence de
l’Assemblée Générale ordinaire ou extraordinaire.

Elle est chargée :

-  de la mise en œuvre des orientations décidées par l’Assemblée
   Générale,
-  de la préparation des bilans, de l’ordre du jour et des propositions
   de modification du règlement intérieur présentés à l’Assemblée
   Générale,
-  de la préparation des propositions de modifications des statuts
   présentés à l’Assemblée Générale extraordinaire. La Collégiale peut
   déléguer tel ou tel de ses pouvoirs, pour une durée déterminée, à une
   ou plusieurs personnes parmi ses volontaires, en conformité avec le
   Règlement Intérieur.

Article 12 - Fonctionnement de la collégiale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La Collégiale s’organise librement mais en toute transparence quant à
ses instances ainsi que sa représentation, toujours sur le critère du
volontariat. La liste des personnes participantes est affichée
publiquement et consultable par toutes et tous. Cette liste, rendue
disponible par Markas en ligne, enregistre l’historique de toute entrée
ou sortie de la Collégiale.

Les dépenses sont ordonnancées par la Collégiale, qui est collectivement
responsable de la comptabilité et décide, en son sein et selon les
besoins, l’octroi du pouvoir de signer tous moyens de paiement ainsi que
les modalités de perception des recettes.

La Trésorerie de l’Association est gérée par la Collégiale, sous l’égide
d’une ou plusieurs personnes adhérentes « payeuses » désignées au
volontariat et en son sein, chargées d’effectuer les transactions
financières validées par la Collégiale. Comme toutes les fonctions liées
à l’administration de l’association, la fonction de personne « payeuse »
peut être mutualisée et tournante. L’Assemblée Générale sera tenue
informée de tout changement par la voie habituelle.

La Collégiale désigne parmi les personnes volontaires présentes les
Secrétaires chargés en particulier de rédiger les procès-verbaux des
réunions de la Collégiale et de l’Assemblée Générale et de tenir le
registre prévu par la loi.

Article 13 - Les Assemblées Générales
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’Assemblée Générale Ordinaire ou Extraordinaire comprend toutes les
personnes adhérant à l’Association, quel que soit leur régime, sous
réserve qu’elles soient à jour de leur cotisation de l’année en cours.

Quinze jours au moins avant la date fixée par la Collégiale, toutes les
personnes adhérentes et affiliées de l’Association sont convoqués par
les soins de la Collégiale. L’ordre du jour est indiqué sur les
convocations. L’Assemblée se réunit sous l’égide de la Collégiale.

Les personnes adhérentes ou affiliées peuvent assister à une Assemblée
par voie électronique conformément aux dispositions prévues par le
règlement intérieur.

Les personnes adhérentes ou affiliées en faisant la demande par écrit
pourront se faire représenter par une autre personne adhérente ou
affiliée, ou voter par anticipation.

Les votes peuvent être faits par voie électronique suivant les
dispositions du règlement intérieur.

Article 14 - L’Assemblée Générale Ordinaire
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’Assemblée Générale se réunit obligatoirement une fois par an. Lors de
cette réunion dite « annuelle », la Collégiale soumet à l’Assemblée un
rapport sur l’activité de l’Association, ainsi que le rapport financier
comportant les comptes de l’exercice écoulé. Il est ensuite procédé à
l’accueil et la bienvenue des nouvelles personnes adhérentes et
affiliées présentes, ainsi que l’enregistrement éventuel de leur volonté
de participation à la Collégiale.

Il est ensuite procédé à l’examen des autres questions figurant à
l’ordre du jour. L’Assemblée Générale Ordinaire peut également être
convoquée à tout moment à la demande de la Collégiale. Les décisions
sont prises à la majorité absolue des suffrages exprimés par les
personnes adhérentes et affiliées présentes ou représentées.

Article 15 - L’Assemblée Générale Extraordinaire
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’Assemblée Générale Extraordinaire se prononce sur les modifications à
apporter aux Statuts et sur la dissolution de l’Association. Elle se
réunit à la demande de la Collégiale.

L’Assemblée Générale Extraordinaire ne peut se prononcer valablement que
si les deux-tiers des personnes adhérentes et affiliées de l’Association
sont présentes ou représentées.

Si ce quorum n’est pas atteint, l’assemblée extraordinaire est convoquée
à nouveau, à quinze jours d’intervalle. Elle peut alors délibérer quel
que soit le nombre de personnes présentes et représentées.

Les décisions sont prises à la majorité des deux-tiers des suffrages
exprimés par les personnes adhérentes et affiliés présentes ou
représentées.

Article 16 - Réglement intérieur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Un règlement intérieur est établi par la Collégiale qui le fait
approuver par l’Assemblée Générale. Ce règlement éventuel est destiné à
fixer les divers points non prévus par les Statuts, notamment ceux qui
ont trait à l’administration interne de l’Association.

Article 17 - Dissolution
^^^^^^^^^^^^^^^^^^^^^^^^

En cas de dissolution prononcée par l’Assemblée Générale Extraordinaire,
une ou plusieurs personnes liquidatrices sont nommées par celle-ci.
L’actif, s’il y a lieu, est dévolu par cette Assemblée à une ou
plusieurs Associations ayant un objet similaire ou à tout établissement
à but social ou culturel de son choix.

