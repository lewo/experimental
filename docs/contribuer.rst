.. _contribuer:

Contribuer à markas.fr
======================

Le dépôt contenant le code de l’infrastructure markas.fr est
https://framagit.org/markas/infrastructure

Ce dépôt contient également des
`tickets <https://framagit.org/markas/infrastructure/-/issues>`__
décrivant les prochaines tâches à réaliser.

Prérequis
---------

L’infrastructure repose fortement sur l’outil `Nix
<https://nixos.org/nix/>`__ et la distribution NixOS. Pour contribuer,
il faut donc installer le gestionnaire de paquet Nix. Ce gestionnaire
de paquet fonctionne sur toutes les distributions en n’entre pas en
conflit avec les paquets de votre distribution.

Pour installer Nix:

::

   curl https://nixos.org/nix/install | sh

Si besoin, le `manuel
Nix <https://nixos.org/nix/manual/#chap-quick-start>`__ fournit plus
d’information et d’options d’installation.

.. _essayer:

Essayer le serveur localement
-----------------------------

Il est possible de lancer construire et démarrer une machine virtuelle
correspondant à la configuration du serveur. Cette machine peut être
utilisée pour tester un changement ou étudier la configuration du
serveur.

.. warning:: `KVM
   <https://fr.wikipedia.org/wiki/Kernel-based_Virtual_Machine>`__ est
   nécessaire pour faire fonctionner la machine virtuelle et doit donc
   être utilisable par votre utilisateur courant. `Cette documentation
   Ubuntu <https://doc.ubuntu-fr.org/kvm>`__ fournies les
   informations nécessaires à l’activation de KVM.

Pour démarrer le serveur localement

::

   ./start-infrastructure


Ce script lance une machine virtuelle, puis une instance
Firefox. Cette instance Firefox permet d'accèder aux services web
hebergés par la machine virtuelle (via un proxy SOCKS).
   
Une fois la machine virtuelle démarrée, il est possible de
s’authentifier avec le compte ``root`` et un mot de passe vide.

Les services suivant sont accessibles directement depuis la machine
hôte:

-  graphana: http://localhost:3000 (utiliser le username ``admin`` et le
   password ``admin``)
-  prometheus server: http://localhost:9090
-  prometheus node exporter: http://localhost:9100
-  ssh: ``ssh -p 2222 root@localhost``
-  nextcloud: http://localhost:30443. Plusieurs comptes admin sont créés
   dont le compte admin (username ``admin`` password ``admin``).

Note: La machine virtuelle ne démarre pas si les ports 3000, 9090, 9100
de la machine hôte sont utilisés.

Tester un changement ou une Merge Request
-----------------------------------------

Des `tests
NixOS <https://nixos.org/nixos/manual/index.html#sec-nixos-tests>`__
sont disponibles pour valider certaines fonctionnalités du serveur. Pour
jouer ces tests:

::

   nix-build -A tests

La documentation explique également comment `jouer ces tests
intéractivement <https://nixos.org/nixos/manual/index.html#sec-running-nixos-tests-interactively>`__.

Soumettre une modification du serveur
-------------------------------------

Le serveur applique toute les minutes la configuration définie dans la
branche master de ce dépôt. Pour appliquer une modification, il faut
donc soumettre une Merge Request (MR) à destination de la branche
master. Une fois que cette MR est mergée, le serveur se met à jour une
minute plus tard.

Lorsque ce dépôt est récupéré, la signature du commit ``HEAD`` est
vérifiée avec les clés publiques contenues dans le répertoire
`./keys <https://framagit.org/markas/infrastructure/-/tree/master/keys>`__.
La configuration est alors uniquement déployée si la signature est
valide. Cela permet de s’assurer qu’un attaquant ne puisse pas
compromettre le dépôt de configuration.

Contribuer à la documentation
-----------------------------

La documentation se trouve dans `./docs
<https://framagit.org/markas/infrastructure/-/tree/master/docs>`__. Elle est
écrite en *RST* (`reStructuredText
<https://fr.wikipedia.org/wiki/ReStructuredText>`_) et rendue par `Sphinx
<https://www.sphinx-doc.org/en/master/index.html>`_. Pour générer le site
statique :

::

   nix-build -A documentation
   firefox ./result/index.html

Il suffit ensuite modifier un fichier et relancer cette commande. Une
Merge Request peut ensuite être créée avec les modifications.

Utilisation du langage de balisage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour connaître la syntaxe du langage *RST*, se référer à la `documentation
officielle
<https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#>`_.

Pour marquer les sections, il est préférable de suivre `ces recommandations
<https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#sections>`_
dans le choix des caractères utilisés pour le soulignage. Les fichiers étant
relativement courts, sans nécessité de recourir à des "parties" et "chapitres",
on considère cette convention :

.. table::
  :align: center

  ====================== =========
  Niveau de titre \*     Caractère
  ====================== =========
  1                      ``=``
  2                      ``-``
  3                      ``^``
  4                      ``"``
  ====================== =========

\* Le niveau de titre s'entend à l'échelle du fichier, et non de la documentation entière.

Langue utilisée
---------------

Les utilisateurs de Markas étant français, nous privilégions cette
langue afin de permettre à nos utilisateurs de suivre ce projet. Nous
utilisons cependant une miriade de projets open source dans lesquels
la langue utilisée est l'anglais. Parce que nous souhaitons reverser
autant que possible notre code à ces communautés, il est préférable
d'écrire ce code en anglais.

Nous faisons donc le compromis suivant: tout ce qui n'est pas
partageable est prioritairement en français; ce qui est partageable
est prioritairement en anglais mais toute contribution en français est
la bienvenue.

Concrètement, est prioritairement en français:

- la documentation
- les README
- les message de commits
- les tickets
- les documents internes

Nous privilégions l'anglais uniquement pour le code et ses
commentaires. Le français reste néanmoins possible afin de ne
restreindre aucun contributeur.
